﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using basiccounter;
using basiccounterLibrary;

namespace UnitTestbasiccounter
{
    [TestClass]
    public class CounterTest
    {
        [TestMethod]
        public void TestincrementTotal()
        {
            Counter compteur = new Counter(0);
            compteur.incrementTotal();
            Assert.AreEqual(1,compteur.getTotal());
            compteur.incrementTotal();
            Assert.AreEqual(2, compteur.getTotal());

        }
        [TestMethod]
        public void TestdecrementTotal()
        {
            Counter compteur = new Counter(0);
            compteur.decrementTotal();
            Assert.AreEqual(0, compteur.getTotal());
            Counter compteur2 = new Counter(3);
            compteur2.decrementTotal();
            Assert.AreEqual(2, compteur2.getTotal());
            compteur2.decrementTotal();
            Assert.AreEqual(1, compteur2.getTotal());

        }
        [TestMethod]
        public void TestresetTotal()
        {
            Counter compteur = new Counter(0);
            compteur.resetTotal();
            Assert.AreEqual(0, compteur.getTotal());
            Counter compteur2 = new Counter(1);
            compteur.resetTotal();
            Assert.AreEqual(0, compteur2.getTotal());
        }

    }
}
