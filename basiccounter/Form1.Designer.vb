﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_decrement = New System.Windows.Forms.Button()
        Me.Btn_increment = New System.Windows.Forms.Button()
        Me.Btn_reset = New System.Windows.Forms.Button()
        Me.Lbl_total = New System.Windows.Forms.Label()
        Me.Lbl_affichetot = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Btn_decrement
        '
        Me.Btn_decrement.Location = New System.Drawing.Point(89, 196)
        Me.Btn_decrement.Name = "Btn_decrement"
        Me.Btn_decrement.Size = New System.Drawing.Size(75, 23)
        Me.Btn_decrement.TabIndex = 0
        Me.Btn_decrement.Text = "-"
        Me.Btn_decrement.UseVisualStyleBackColor = True
        '
        'Btn_increment
        '
        Me.Btn_increment.Location = New System.Drawing.Point(567, 191)
        Me.Btn_increment.Name = "Btn_increment"
        Me.Btn_increment.Size = New System.Drawing.Size(75, 23)
        Me.Btn_increment.TabIndex = 1
        Me.Btn_increment.Text = "+"
        Me.Btn_increment.UseVisualStyleBackColor = True
        '
        'Btn_reset
        '
        Me.Btn_reset.Location = New System.Drawing.Point(311, 336)
        Me.Btn_reset.Name = "Btn_reset"
        Me.Btn_reset.Size = New System.Drawing.Size(115, 23)
        Me.Btn_reset.TabIndex = 2
        Me.Btn_reset.Text = "Remise à zéro"
        Me.Btn_reset.UseVisualStyleBackColor = True
        '
        'Lbl_total
        '
        Me.Lbl_total.AutoSize = True
        Me.Lbl_total.Location = New System.Drawing.Point(332, 75)
        Me.Lbl_total.Name = "Lbl_total"
        Me.Lbl_total.Size = New System.Drawing.Size(31, 13)
        Me.Lbl_total.TabIndex = 3
        Me.Lbl_total.Text = "Total"
        '
        'Lbl_affichetot
        '
        Me.Lbl_affichetot.AutoSize = True
        Me.Lbl_affichetot.Location = New System.Drawing.Point(350, 201)
        Me.Lbl_affichetot.Name = "Lbl_affichetot"
        Me.Lbl_affichetot.Size = New System.Drawing.Size(13, 13)
        Me.Lbl_affichetot.TabIndex = 4
        Me.Lbl_affichetot.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Lbl_affichetot)
        Me.Controls.Add(Me.Lbl_total)
        Me.Controls.Add(Me.Btn_reset)
        Me.Controls.Add(Me.Btn_increment)
        Me.Controls.Add(Me.Btn_decrement)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btn_decrement As Button
    Friend WithEvents Btn_increment As Button
    Friend WithEvents Btn_reset As Button
    Friend WithEvents Lbl_total As Label
    Friend WithEvents Lbl_affichetot As Label
End Class
