﻿Imports basiccounterLibrary

Public Class Form1
    Dim counter As New Counter(0)

    Private Sub Btn_decrement_Click(sender As Object, e As EventArgs) Handles Btn_decrement.Click
        counter.decrementTotal()
        Lbl_affichetot.Text = counter.getTotal()

    End Sub

    Private Sub Btn_reset_Click(sender As Object, e As EventArgs) Handles Btn_reset.Click
        counter.resetTotal()
        Lbl_affichetot.Text = counter.getTotal()
    End Sub

    Private Sub Btn_increment_Click(sender As Object, e As EventArgs) Handles Btn_increment.Click
        counter.incrementTotal()
        Lbl_affichetot.Text = counter.getTotal()
    End Sub

End Class
