﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace basiccounterLibrary
{
    public class Counter
    {
        private int total =0;
        public Counter (int total)
        {
            this.total = total; 
        }

        public void incrementTotal()
        {
            this.total = this.total + 1;
         
        }

        public void decrementTotal()
        {
            if (this.total > 0)
            {
                this.total = this.total - 1;
            }
           
        }

        public void resetTotal()
        {
            this.total = 0;
           
        }

        public int getTotal()
        {
            return this.total;
        }
    }
}
